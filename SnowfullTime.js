/**
 * Created by WebNaut
 */


(function ($){
    $.snowfullTime = function (el, options) {
        this.options = $.extend({},$.snowfullTime.defaultOptions, options);

        var mobileWidth = 776;

        function init () {
            startItUp();
        }

        function startItUp () {
            makeFlakes();
        }

        function makeFlakes() {
            var flake_elem;
            if ($(window).width() <= mobileWidth)
                var flakeCount = options.flakeCountMobile;
            else
                var flakeCount = options.flakeCount;

            for ($i = $("." + options.flakeName).size(); $i < flakeCount; $i++) {
                flake_elem = $("<div class='" + options.flakeName + "'></div>");
                $(el).append(flake_elem);
                flake_elem.attr("data-speed", randomNum(options.snowingSpeedMin, options.snowingSpeedMax));

                var flakeSize = randomNum(options.flakeSizeMin, options.flakeSizeMax);

                flake_elem.css({
                    'width': flakeSize + "px",
                    'height': flakeSize + "px"
                });
                flake_elem.css({
                    'left': randomNum(0, $(window).width()) + "px",
                    'top': "-" + (flake_elem.height()+10) + "px"
                });

                flake_elem.delay(randomNum(1000, 8000)).animate({
                    'top': parseInt($(window).height()-randomNum(1, (parseInt($(window).height()*40%100)))) + "px"
                }, parseInt(flake_elem.attr("data-speed")), 'linear', function() {
                    $(this).animate({
                        'opacity': 0
                    }, function() {
                        deleteFlake($(this));
                    });
                });
            }
        }

        function deleteFlake(flake) {
            flake.remove();
            makeFlakes();
        }

        var randomNum = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };



        init();
    };

    $.snowfullTime.defaultOptions = {
        flakeName: 'snowfull-flake',
        flakeCount: 70,
        flakeCountMobile: 30,
        snowingSpeedMin: 5000,
        snowingSpeedMax: 10000,
        flakeSizeMin: 1,
        flakeSizeMax: 13
    };

    $.fn.snowfullTime = function (options) {
        return this.each(function () {
            (new $.snowfullTime(this, options));
        });
    };

})(jQuery);