### Great snowing jQuery plugin ###

* Author: Lukas Materna
* Version: 1.0

### Usage ###


```
#!javascript

$.snowfullTime('body', {
        flakeName: 'snowfull-flake',
        flakeCount: 70,
        flakeCountMobile: 20,
        snowingSpeedMin: 5000,
        snowingSpeedMax: 10000,
        flakeSizeMin: 1,
        flakeSizeMax: 13
    });

```